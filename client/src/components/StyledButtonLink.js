import styled from 'styled-components';

const StyledButtonLink = styled.button`
	padding-top: 1rem;
	background: none;
	color: inherit;
	border: none;
	outline: none;
	font: inherit;
	cursor: pointer;
	transition: 0.2s;
	opacity: 0.6;
	&:hover {
		opacity: 1;
	}
	&:active {
		color: green;
		opacity: 1;
	}
`;

export default StyledButtonLink;

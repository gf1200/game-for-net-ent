import React from 'react';
import styled from 'styled-components';

const Container = ({ className, children }) => <div className={className}>{children}</div>;

const ContainerStyled = styled(Container)`
	flex-basis: 100%;
	height: 100%;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
	padding: 1rem;
	background-color: #e9e7e0;
	/* border: white solid 0.25rem; */
	/* border-radius: 1rem; */
	/* box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); */

	@media screen and (max-width: 640px) and (max-height: 448px) {
	}
`;

export default ContainerStyled;

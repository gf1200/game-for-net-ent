import React from 'react';
import { Emojione } from 'react-emoji-render';
import styled from 'styled-components';

const NotificationStyled = styled.div`
  font-size: 1rem;
  line-height: 1.5rem;
  text-align: center;
  margin: 1rem 0;
  color: #424249;
  height: 5rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  @media screen and (max-width: 640px) and (max-height: 448px) {
    margin: 0.5rem 0;
  }
`;

const Notifications = ({ roundType, winInThisRound, isFetching, errors }) => {
  return (
    <NotificationStyled>
      {winInThisRound == null && !errors && (
        <div>
          <Emojione svg className="emoji" text="👋" />{' '}
          <span>
            Hey. You have 10,000 coins for a good start.
            <br /> Pay 10 for a bet and try to win more
          </span>
        </div>
      )}
      {isFetching && !errors && (
        <div>
          <Emojione svg className="emoji" text="⏳" /> <span>Random</span>
        </div>
      )}

      {winInThisRound && !errors && (
        <div>
          <Emojione svg className="emoji" text="💰" />{' '}
          <span>Congratulations! You win +20 coins.</span>
        </div>
      )}
      {!roundType && !isFetching && !errors && (
        <div>
          <Emojione svg className="emoji" text="🎉" />{' '}
          <span>You win a free bet!</span>
        </div>
      )}
      {winInThisRound !== null && !winInThisRound && !isFetching && !errors && (
        <div>
          <Emojione svg className="emoji" text="💪" /> <span>Keep trying</span>
        </div>
      )}
      {errors && (
        <div>
          <Emojione svg className="emoji" text="💩" />{' '}
          <span>{errors} Something went wrong</span>
        </div>
      )}
    </NotificationStyled>
  );
};

export default Notifications;

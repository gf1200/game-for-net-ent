import React from 'react';
import styled from 'styled-components';

const Budget = ({ className, children }) => <div className={className}>{children}</div>;

const BudgetStyled = styled(Budget)`
	width: 100%;
	display: flex;
	justify-content: center;
`;

export default BudgetStyled;

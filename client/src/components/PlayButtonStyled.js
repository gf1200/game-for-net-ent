import React from 'react';
import styled from 'styled-components';

const ButtonStyled = styled.button`
	background: ${props => (props.isFree ? '#9EE6AA' : '#re0dad5')};
	color: #424249;
	font-family: 'Saira', sans-serif;
	font-weight: bold;
	width: 280px;
	height: 4rem;
	border: white solid 0.25rem;
	border-radius: 1rem;
	outline: none;
	transition: 0.2s;
	&:hover {
		border: #424249 solid 0.25rem;
	}
	&:active {
		border: green solid 0.25rem;
	}
	@media screen and (max-width: 640px) and (max-height: 448px) {
		height: 2.5rem;
		width: 50%;
	}
`;

const PlayButtonStyled = ({ className, onClickButton, roundType, isFetching }) => {
	return (
		<ButtonStyled
			className={className}
			onClick={() => onClickButton()}
			disabled={isFetching}
			isFree={!roundType && !isFetching}
		>
			{!roundType && !isFetching ? `Free bet` : 'New bet'}
		</ButtonStyled>
	);
};

export default PlayButtonStyled;
//e0dad5

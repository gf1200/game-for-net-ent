import React, { Component } from 'react';
import EmojiItem from './EmojiItem';
import styled from 'styled-components';

const StyledEmojiItems = styled.div`
	font-size: 3rem;
	margin-top: 1rem;
	padding: 1rem 0;
	border-top: #e0dad5 solid 2px;
	border-bottom: #e0dad5 solid 2px;
	.emoji {
		padding: 0 0.5rem;
		border-right: #e0dad5 solid 2px;
		&:last-child {
			border: none;
		}
	}
	@media screen and (max-width: 640px) and (max-height: 448px) {
		font-size: 2rem;
		margin-top: 0.75rem;
		padding: 0.75rem 0;
	}
`;

export default class MachineScreen extends Component {
	state = {
		numbersForEmojisScreen: []
	};

	componentWillMount() {
		this.slotScreenControler();
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isFetching !== this.props.isFetching) {
			if (nextProps.isFetching === true) {
				this.interval = setInterval(() => this.slotScreenControler('random'), 100);
			}
			if (nextProps.isFetching === false) {
				this.interval = clearInterval(this.interval);
			}
		}
		if (nextProps.winInThisRound === true) {
			this.slotScreenControler();
		}
	}

	interval = null;

	getFloorNumber(limit) {
		return Math.floor(Math.random() * limit);
	}

	slotScreenControler(type) {
		const constNumber = this.getFloorNumber(12);
		var arrOfUniqueNumbers = [];
		while (arrOfUniqueNumbers.length < 3) {
			switch (type) {
				case 'random':
					var r = this.getFloorNumber(12);
					if (arrOfUniqueNumbers.indexOf(r) === -1) arrOfUniqueNumbers.push(r);
					break;

				default:
					arrOfUniqueNumbers.push(constNumber);
					break;
			}
		}

		this.setState({ numbersForEmojisScreen: arrOfUniqueNumbers });
	}

	render() {
		const { numbersForEmojisScreen } = this.state;
		let emojisScreem;
		if (numbersForEmojisScreen) {
			emojisScreem = numbersForEmojisScreen.map((number, index) => {
				return <EmojiItem key={index} emojiNr={number} />;
			});
		}

		return <StyledEmojiItems>{emojisScreem}</StyledEmojiItems>;
	}
}

import React from 'react';
import { Emojione } from 'react-emoji-render';

import styled from 'styled-components';

const StyledHeader = styled.h1`
  font-size: 2rem;
  color: #424249;
  font-family: 'Saira', sans-serif;
  .emoji {
    font-size: 1.4rem;
    position: relative;
    top: -0.2rem;
  }
`;

function Budget({ coins }) {
  return (
    <StyledHeader>
      {coins} <Emojione svg className="emoji" text="💰" />
    </StyledHeader>
  );
}

export default Budget;

import React from 'react';
import { Emojione } from 'react-emoji-render';

const EmojiItem = ({ emojiNr }) => {
  const allEmojis = [
    {
      symbol: '🦊',
      label: 'Fox'
    },
    {
      symbol: '🐴',
      label: 'Horse'
    },
    {
      symbol: '🐕',
      label: 'Dog'
    },
    {
      symbol: '🔥',
      label: 'Fire'
    },
    {
      symbol: '💥',
      label: 'Collision'
    },
    {
      symbol: '🌈',
      label: 'Rainbow'
    },
    {
      symbol: '🍋',
      label: 'Lemon'
    },
    {
      symbol: '🌶️',
      label: 'Hot Pepper'
    },
    {
      symbol: '🍄',
      label: 'Mushroom'
    },
    {
      symbol: '💣',
      label: 'Bomb'
    },
    {
      symbol: '⏰',
      label: 'Alarm Clock'
    },
    {
      symbol: '🐖',
      label: 'Peeg'
    }
  ];
  return <Emojione svg className="emoji" text={allEmojis[emojiNr].symbol} />;
};

export default EmojiItem;

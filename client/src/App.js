import React, { Component } from 'react';
import axios from 'axios';

import ContainerStyled from './components/ContainerStyled';
import BudgetStyled from './components/BudgetStyled';
import Budget from './components/Budget';
import PlayButtonStyled from './components/PlayButtonStyled';
import MachineScreen from './components/MachineScreen';
import Notifications from './components/Notifications';
import StyledButtonLink from './components/StyledButtonLink';

class App extends Component {
  state = {
    totalCoins: '----',
    numbersForEmojisScreen: [],
    normalRound: true,
    winInThisRound: null,
    fetchingData: false
  };

  componentDidMount() {
    this.newGame();
  }

  newGame = () => {
    axios
      .get('/api/pleyer/new-game')
      .then(res => {
        this.setState(state => ({
          ...state,
          ...res.data,
          winInThisRound: null,
          error: null
        }));
      })
      .catch(error => {
        this.setState({ error: error.request.status });
      });
  };

  newBet = () => {
    this.setState({ fetchingData: true, winInThisRound: false });
    axios
      .get('/api/pleyer')
      .then(res => {
        if (this.state.normalRound) {
          this.setState({ totalCoins: this.state.totalCoins - 10 });
        }
        setTimeout(() => {
          this.setState({ fetchingData: false });
          if (res.data.totalCoins > this.state.totalCoins) {
            this.setState({
              winInThisRound: true
            });
          }
          this.setState(state => ({
            ...state,
            ...res.data
          }));
        }, 800);
      })
      .catch(error => {
        this.setState({ error: error.request.status, fetchingData: false });
      });
  };

  render() {
    return (
      <ContainerStyled>
        <BudgetStyled>
          <Budget coins={this.state.totalCoins} />
        </BudgetStyled>
        <MachineScreen
          isFetching={this.state.fetchingData}
          winInThisRound={this.state.winInThisRound}
        />
        <Notifications
          roundType={this.state.normalRound}
          isFetching={this.state.fetchingData}
          winInThisRound={this.state.winInThisRound}
          errors={this.state.error}
        />
        <PlayButtonStyled
          onClickButton={this.newBet}
          roundType={this.state.normalRound}
          isFetching={this.state.fetchingData}
        />
        <StyledButtonLink onClick={() => this.newGame()}>
          Restart game
        </StyledButtonLink>
      </ContainerStyled>
    );
  }
}

export default App;

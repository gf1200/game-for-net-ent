const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const session = require('express-session');

const pleyer = require('./routes/api/pleyer');

const app = express();

// Bodyparser Middleware
app.use(bodyParser.json());
app.use(
	session({
		secret: 'JagodowyDomek',
		saveUninitialized: false,
		resave: false
	})
);

app.use('/api/pleyer', pleyer);

// Serve static assets if in production
if (process.env.NODE_ENV === 'production') {
	app.use(express.static('client/build'));

	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
	});
}

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));

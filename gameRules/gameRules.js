function randomNumber() {
	return Math.random();
}

module.exports = function(req, res) {
	const { normalRound } = req.session.pleyer;
	const ileMam = req.session.pleyer.totalCoins;

	if (normalRound) {
		req.session.pleyer.totalCoins -= 10;
	}

	if (randomNumber() <= 0.3) {
		req.session.pleyer.totalCoins += 20;
	}

	if (!normalRound) {
		req.session.pleyer.normalRound = true;
	}

	if (randomNumber() <= 0.1) {
		req.session.pleyer.normalRound = false;
	}

	res.json(req.session.pleyer);
};

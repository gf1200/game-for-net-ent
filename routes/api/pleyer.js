const express = require('express');
const router = express.Router();

const initialData = require('../../gameRules/initialData');
const gameRules = require('../../gameRules/gameRules');

router.get('/', (req, res) => {
	if (req.session.pleyer) {
		gameRules(req, res);
	} else {
		req.session.pleyer = initialData;
		res.json(req.session.pleyer);
	}
});

router.get('/new-game', (req, res) => {
	req.session.pleyer = initialData;
	res.json(req.session.pleyer);
});

module.exports = router;
